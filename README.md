# inject

Simple DLL injection tool written in Rust.

## Usage

```
Injects the DLL at PATH into a PROCESS.

USAGE:
    inject [FLAGS] [OPTIONS] <PATH> <PROCESS>

FLAGS:
    -h, --help        Prints help information
    -i, --infinite    Whether to loop and re-inject if the target process is closed. Overrides `retry`.
    -r, --retry       Whether to continuously retry if target process is not found.
        --silent      Disables info log output, only displaying errors. Overrides --verbose.
    -V, --version     Prints version information
    -v, --verbose     Pass many times for more log output
                      
                      By default, it'll report info. Passing `-v` one time also prints debug, `-vv` enables trace.

OPTIONS:
    -p, --pre <pre>      Number of milliseconds to wait prior to injecting. [default: 0]
    -w, --wait <wait>    Number of seconds to wait before retrying. Must be larger than 0. [default: 1]

ARGS:
    <PATH>       Path to the dll to inject.
    <PROCESS>    Name of the process to inject into.
```