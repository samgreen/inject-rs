//! Simple Windows DLL injection tool.

#![cfg(windows)]

#[macro_use]
extern crate failure;
extern crate inject;
extern crate structopt;
#[macro_use]
extern crate log;
extern crate simplelog;
extern crate winproc;

use failure::{Error, ResultExt};
use inject::{inject_dll, is_named_process_injected, is_process_injected, named_inject_dll};
use std::{
    io::{self, prelude::*},
    path::PathBuf,
    process,
    thread,
    time::Duration,
};
use structopt::StructOpt;
use winproc::Process;

/// Injects the DLL at PATH into a PROCESS.
#[derive(Debug, StructOpt)]
#[structopt(author = "")]
struct Opt {
    /// Path to the dll to inject.
    #[structopt(name = "PATH", parse(from_os_str))]
    path: PathBuf,
    /// Name of the process to inject into.
    #[structopt(name = "PROCESS")]
    process: String,
    /// Whether to continuously retry if target process is not found.
    #[structopt(short = "r", long = "retry")]
    retry: bool,
    /// Whether to loop and re-inject if the target process is closed. Overrides `retry`.
    #[structopt(short = "i", long = "infinite")]
    infinite: bool,
    /// Number of seconds to wait before retrying.
    #[structopt(short = "w", long = "wait", default_value = "1")]
    wait: f64,
    /// Number of milliseconds to wait prior to injecting.
    #[structopt(short = "p", long = "pre", default_value = "0")]
    pre: u64,
    /// Pass many times for more log output
    ///
    /// By default, it'll report info. Passing `-v` one time also prints
    /// debug, `-vv` enables trace.
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,
    /// Disables info log output, only displaying errors. Overrides --verbose.
    #[structopt(long = "silent")]
    silent: bool,
}

const FAILED: &str = "Failed to inject DLL.";

pub fn duration_from_float_secs(secs: f64) -> Duration {
    const NANOS_PER_SEC: u32 = 1_000_000_000;
    const MAX_NANOS_F64: f64 = ((u64::max_value() as u128 + 1) * (NANOS_PER_SEC as u128)) as f64;
    let nanos = secs * (NANOS_PER_SEC as f64);
    if !nanos.is_finite() {
        panic!("got non-finite value when converting float to duration");
    }
    if nanos >= MAX_NANOS_F64 {
        panic!("overflow when converting float to duration");
    }
    if nanos < 0.0 {
        panic!("underflow when converting float to duration");
    }
    let nanos = nanos as u128;
    Duration::new(
        (nanos / (NANOS_PER_SEC as u128)) as u64,
        (nanos % (NANOS_PER_SEC as u128)) as u32,
    )
}

fn run() -> Result<(), Error> {
    let mut opt: Opt = Opt::from_args();

    let log_level = if opt.silent {
        simplelog::LevelFilter::Error
    } else {
        match opt.verbose {
            0 => simplelog::LevelFilter::Info,
            1 => simplelog::LevelFilter::Debug,
            _ => simplelog::LevelFilter::Trace,
        }
    };
    simplelog::TermLogger::init(log_level, Default::default())?;

    if opt.infinite {
        opt.retry = true;
    }
    let pre = Duration::from_millis(opt.pre);
    ensure!(opt.wait >= 0.0, "wait must be positive.");
    ensure!(
        opt.path.is_file(),
        "{} does not exist.",
        opt.path.to_string_lossy()
    );

    let interval = duration_from_float_secs(opt.wait);
    let err_msg = format!(
        "No process found with name {}, retrying in {} seconds.\n",
        opt.process, opt.wait
    );

    if opt.retry {
        loop {
            match Process::from_name(&opt.process) {
                Ok(process) => {
                    if opt.pre > 0 {
                        info!(
                            "Process found. Injecting in {}ms.",
                            pre.as_secs() * 1000 + pre.subsec_millis() as u64
                        );
                    } else {
                        info!("Process found. Injecting.");
                    }
                    thread::sleep(pre);
                    match inject_dll(&opt.path, &process) {
                        Ok(()) => {
                            if !is_process_injected(&opt.path, &process).unwrap_or(false) {
                                error!("Failed to inject DLL. Not found in loaded modules.");
                                continue;
                            } else {
                                info!(
                                    "Injected {}",
                                    opt.path.file_name().unwrap().to_string_lossy()
                                );
                            }
                            if opt.infinite {
                                while process.is_running() {
                                    thread::sleep(Duration::from_secs(5))
                                }
                                continue;
                            } else {
                                break;
                            }
                        }
                        Err(inject::Error::Windows(winproc::Error::NoProcess(_))) => {
                            let _ = io::stderr().write_all(err_msg.as_bytes());
                        }
                        e => e.context(FAILED)?,
                    }
                }
                Err(winproc::Error::NoProcess(_)) => {
                    let _ = io::stderr().write_all(err_msg.as_bytes());
                }
                e => {
                    e?;
                }
            }
            thread::sleep(interval);
        }
    } else {
        thread::sleep(pre);
        named_inject_dll(&opt.path, &opt.process).context(FAILED)?;
        if !is_named_process_injected(&opt.path, &opt.process).context(FAILED)? {
            bail!("Failed to inject DLL. Not found in loaded modules.");
        }
    }

    Ok(())
}

fn main() {
    if let Err(e) = run() {
        error!("{}", e);
        for cause in e.iter_causes() {
            error!("Cause: {}", cause);
        }
        process::exit(
            if let Some(code) = e
                .downcast_ref::<io::Error>()
                .and_then(|io_e| io_e.raw_os_error())
            {
                code
            } else {
                1
            },
        );
    }
}
