//! Simple DLL injection library.

#![cfg(windows)]

#[macro_use]
extern crate failure;
extern crate winapi;
extern crate winproc;
#[macro_use]
extern crate const_cstr;

use std::{ffi::CString, mem, os::windows::io::AsRawHandle, path::Path, ptr};
use winapi::{
    shared::minwindef::LPVOID,
    um::{
        libloaderapi::{GetModuleHandleA, GetProcAddress},
        memoryapi::{VirtualAllocEx, VirtualFreeEx, WriteProcessMemory},
        processthreadsapi::CreateRemoteThread,
        synchapi::WaitForSingleObject,
        winbase::INFINITE,
        winnt::{MEM_COMMIT, MEM_RELEASE, MEM_RESERVE, PAGE_READWRITE},
    },
};
use winproc::{Handle, Process};

pub use errors::{Error, InjectResult};

pub mod errors;

pub fn named_inject_dll(dll_path: &Path, process: &str) -> InjectResult {
    inject_dll(dll_path, &Process::from_name(process)?)
}

pub fn inject_dll(dll_path: &Path, process: &Process) -> InjectResult {
    unsafe {
        let load_lib_addr: LPVOID = GetProcAddress(
            GetModuleHandleA(const_cstr!("kernel32.dll").as_ptr()),
            const_cstr!("LoadLibraryA").as_ptr(),
        ) as LPVOID;

        let c_path = CString::new(dll_path.canonicalize()?.to_string_lossy().as_bytes())
            .map_err(|_| Error::NullByte)?;

        let remote_string = VirtualAllocEx(
            process.as_raw_handle(),
            ptr::null_mut(),
            c_path.to_bytes().len(),
            MEM_COMMIT | MEM_RESERVE,
            PAGE_READWRITE,
        );

        WriteProcessMemory(
            process.as_raw_handle(),
            remote_string,
            c_path.as_ptr() as _,
            c_path.to_bytes().len(),
            ptr::null_mut(),
        );

        let thread_h = Handle::new(CreateRemoteThread(
            process.as_raw_handle(),
            ptr::null_mut(),
            0,
            mem::transmute(load_lib_addr),
            remote_string,
            0,
            ptr::null_mut(),
        ));

        WaitForSingleObject(thread_h.as_raw_handle(), INFINITE);

        VirtualFreeEx(
            process.as_raw_handle(),
            remote_string,
            c_path.to_bytes().len(),
            MEM_RELEASE,
        );

        Ok(())
    }
}

pub fn is_named_process_injected(dll_path: &Path, process: &str) -> InjectResult<bool> {
    is_process_injected(dll_path, &Process::from_name(process)?)
}

pub fn is_process_injected(dll_path: &Path, process: &Process) -> InjectResult<bool> {
    for module in process.module_list()? {
        if dll_path
            .file_name()
            .expect("DLL had no name.")
            .to_string_lossy()
            == module.name()?
        {
            return Ok(true);
        }
    }

    Ok(false)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn inject_self() {
        let process = Process::current();
        let manifest_dir = Path::new(env!("CARGO_MANIFEST_DIR"));
        let dll_path = manifest_dir.join(r"resources\test.dll");
        assert!(dll_path.is_file(), "DLL doesn't exist.");
        inject_dll(&dll_path, &process).unwrap();
        assert!(is_process_injected(&dll_path, &process).unwrap());
    }
}
