use std::io;
use winproc;

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "Io error: {}", _0)]
    Io(#[cause] io::Error),
    #[fail(display = "{}", _0)]
    Windows(#[cause] winproc::Error),
    #[fail(display = "Provided string contained a null byte.")]
    NullByte,
}

pub type InjectResult<T = ()> = ::std::result::Result<T, Error>;

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::Io(e)
    }
}

impl From<winproc::Error> for Error {
    fn from(e: winproc::Error) -> Error {
        Error::Windows(e)
    }
}
